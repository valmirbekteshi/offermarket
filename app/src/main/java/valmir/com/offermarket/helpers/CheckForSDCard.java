package valmir.com.offermarket.helpers;

import android.os.Environment;

/**
 * Created by Visari on 6/23/2017.
 */


public class CheckForSDCard {
    //Check If SD Card is present or not method
    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
}
