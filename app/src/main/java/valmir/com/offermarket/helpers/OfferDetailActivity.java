//package valmir.com.offermarket.helpers;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.design.widget.BottomNavigationView;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v4.view.ViewPager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.MenuItem;
//import android.view.View;
//import valmir.com.offermarket.ApiInterface.ApiInterface;
//import valmir.com.offermarket.R;
//import valmir.com.offermarket.fragments.ViewPagerAdapter;
//import valmir.com.offermarket.fragments.offerDetailFragment.GalleryFragment;
//import valmir.com.offermarket.fragments.offerDetailFragment.ActionLocation;
//import valmir.com.offermarket.fragments.offerDetailFragment.OrariFragment;
//import valmir.com.offermarket.fragments.offerDetailFragment.ViewAllFragment;
//import valmir.com.offermarket.rest.ApiClient;
//
//public class OfferDetailActivity extends AppCompatActivity {
//
//    BottomNavigationView bottomNavigationView;
//    private ViewPager viewPager;
//
//    ViewAllFragment viewAllFragment;
//    ActionLocation locationFragment;
//    GalleryFragment galleryFragment;
//    OrariFragment orariFragment;
//
//    MenuItem prevMenuItem;
//    private String offerId;
//    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_offer_detail);
//
//        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
//        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        setupViewPager(viewPager);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        LocalBroadcastManager.getInstance(this)
//            .registerReceiver(mHandler, new IntentFilter("offer_detail"));
//
//        // TODO: 26-Mar-18 CALL API GET OFFER BY ID
//
//        Intent intent = getIntent();
//
//        offerId = intent.getStringExtra("offer_id");
//        intent.putExtra("offer_id",offerId);
//        //fetchOfferById(offerId);
//        //
//        //if (getIntent().getExtras() != null) {
//        //    for (String key : getIntent().getExtras().keySet()) {
//        //
//        //        if (key.equals("offer_id")) {
//        //            // FIXME: 18-Mar-18
//        //            offerId = getIntent().getExtras().getString(key);
//        //        }
//        //    }
//        //}
//
//        bottomNavigationView.setOnNavigationItemSelectedListener(
//            new BottomNavigationView.OnNavigationItemSelectedListener() {
//                @Override
//                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                    switch (item.getItemId()) {
//                        case R.id.action_all:
//                            viewPager.setCurrentItem(0);
//                            break;
//                        case R.id.action_location:
//                            viewPager.setCurrentItem(1);
//                            break;
//                        case R.id.action_gallery:
//                            viewPager.setCurrentItem(2);
//                            break;
//                        case R.id.action_orari:
//                            viewPager.setCurrentItem(3);
//                            break;
//                    }
//
//                    return false;
//                }
//            });
//
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset,
//                int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                if (prevMenuItem != null) {
//                    prevMenuItem.setChecked(false);
//                } else {
//                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
//                }
//
//                bottomNavigationView.getMenu().getItem(position).setChecked(true);
//                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//
//        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
//    }
//
//
//
//
//    private void setupViewPager(ViewPager viewPager)
//    {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewAllFragment = new ViewAllFragment();
//        locationFragment =new ActionLocation();
//        galleryFragment =new GalleryFragment();
//        orariFragment=new OrariFragment();
//
//        adapter.addFragment(viewAllFragment);
//        adapter.addFragment(locationFragment);
//        adapter.addFragment(galleryFragment);
//        adapter.addFragment(orariFragment);
//        viewPager.setAdapter(adapter);
//    }
//
//
//
//
//
//    private BroadcastReceiver mHandler = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            offerId = intent.getStringExtra("offer_id");
//        }
//    };
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
//    }
//}
