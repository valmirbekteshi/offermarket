package valmir.com.offermarket.helpers;

/**
 * Created by Valmir on 28-Mar-18.
 */

public class Var {

  public static double toDouble( String s){
    return Double.parseDouble(s);
  }


  public static boolean notEmpty(String s){
    return !s.isEmpty();
  }
}
