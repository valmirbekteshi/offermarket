package valmir.com.offermarket.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Categories {

	@SerializedName("_id")
	@Expose
	private String id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("categoryID")
	@Expose
	private Integer categoryID;
	@SerializedName("icon")
	@Expose
	private String icon;
	@SerializedName("__v")
	@Expose
	private Integer v;
	@SerializedName("create_date")
	@Expose
	private String createDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getV() {
		return v;
	}

	public void setV(Integer v) {
		this.v = v;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
}