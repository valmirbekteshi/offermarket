package valmir.com.offermarket.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Catalog {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("categoryID")
    @Expose
    private Integer categoryID;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("lontitude")
    @Expose
    private Double lontitude;
    @SerializedName("orariPunes")
    @Expose
    private String orariPunes;
    @SerializedName("nrTel")
    @Expose
    private String nrTel;
    @SerializedName("ofertuesi")
    @Expose
    private String ofertuesi;
    @SerializedName("skadon")
    @Expose
    private String skadon;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("isFav")
    @Expose
    private Boolean isFav;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLontitude() {
        return lontitude;
    }

    public void setLontitude(Double lontitude) {
        this.lontitude = lontitude;
    }

    public String getOrariPunes() {
        return orariPunes;
    }

    public void setOrariPunes(String orariPunes) {
        this.orariPunes = orariPunes;
    }

    public String getNrTel() {
        return nrTel;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    public String getOfertuesi() {
        return ofertuesi;
    }

    public void setOfertuesi(String ofertuesi) {
        this.ofertuesi = ofertuesi;
    }

    public String getSkadon() {
        return skadon;
    }

    public void setSkadon(String skadon) {
        this.skadon = skadon;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Boolean getIsFav() {
        return isFav;
    }

    public void setIsFav(Boolean isFav) {
        this.isFav = isFav;
    }

}

