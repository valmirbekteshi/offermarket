package valmir.com.offermarket.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.bumptech.glide.request.RequestOptions;
import java.util.List;

import valmir.com.offermarket.model.Categories;
import valmir.com.offermarket.R;

/**
 * Created by Besplay on 3/13/2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyProductHolder> {

  private List<Categories> categoriesList;
  private Context context;

  public static class MyProductHolder extends RecyclerView.ViewHolder {

    private ImageView categoriesIcon;
    //private TextView categoriesName;

    public MyProductHolder(View itemView) {
      super(itemView);

      categoriesIcon = (ImageView) itemView.findViewById(R.id.categoriesIcon);
     // categoriesName = (TextView) itemView.findViewById(R.id.categoriesName);
    }
  }

  public CategoriesAdapter(List<Categories> categoriesList, Context context) {
    this.categoriesList = categoriesList;
    this.context = context;
  }

  @Override
  public MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_item_categories, parent, false);
    return new MyProductHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final MyProductHolder holder, int position) {
    final Categories product = categoriesList.get(position);

    //        Typeface openSansRegular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    //        Typeface openSansBold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
    //        Typeface openSansSemiBold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");

    Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");

    //this is the original Google Drive link to the image
    //String s= product.getIcon();
    //you have to get the part of the link 0B9nFwumYtUw9Q05WNlhlM2lqNzQ
    // String[] p=s.split("/");
    //Create the new image link
    //        String imageLink="https://drive.google.com/uc?export=download&id="+p[5];

    String imageLink = product.getIcon();

    Glide
        .with(context)
        .load(imageLink)
        .apply(new RequestOptions().placeholder(R.drawable.img_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.ALL))
        .into(holder.categoriesIcon);
//
//    holder.categoriesName.setText(product.getName());
//
//    holder.categoriesName.setTypeface(AEH);
  }

  @Override
  public int getItemCount() {
    return categoriesList.size();
  }
}
