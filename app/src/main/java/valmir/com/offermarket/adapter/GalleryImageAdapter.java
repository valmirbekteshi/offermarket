//package valmir.com.offermarket.adapter;
//
///**
// * Created by Valmir on 31-Jan-18.
// */
//
//import android.content.Context;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.GridView;
//import android.widget.ImageView;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
//import java.util.List;
//import valmir.com.offermarket.R;
//
//public class GalleryImageAdapter extends BaseAdapter {
//    private Context mContext;
//    List<String> imgUrls
//
//
//    // Constructor
//    public GalleryImageAdapter(Context c,List<String> imgUrls ){
//        mContext = c;
//        this.imgUrls = imgUrls;
//    }
//
//
//
//    @Override
//    public int getCount() {
//        return imgUrls.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return imgUrls.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//
//
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        String pic = imgUrls.get(position);
//
//        Glide
//            .with(mContext)
//            .load(pic)
//            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//            .into(convertView);
//
//        imageView.setImageResource(imgUrls[position]);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        imageView.setLayoutParams(new GridView.LayoutParams(130, 100));
//        return imageView;
//    }
//
//}