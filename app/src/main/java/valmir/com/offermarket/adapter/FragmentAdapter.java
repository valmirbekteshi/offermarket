package valmir.com.offermarket.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import valmir.com.offermarket.fragments.CategoriesFragment;
import valmir.com.offermarket.fragments.HartaFragment;
import valmir.com.offermarket.fragments.OffersFragment;


/**
 * Created by Valmir on 12-Nov-17.
 */

public class FragmentAdapter extends FragmentPagerAdapter {

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new OffersFragment();
            case 1:
                return new CategoriesFragment();

            case 2:
                return new HartaFragment();


        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            //
            //Your tab titles
            //
            case 0:return "Ofertat";
            case 1:return "Kategoritë";
            case 2:return "Harta";
            default:return null;
        }
    }
}