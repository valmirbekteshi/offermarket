package valmir.com.offermarket.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.request.RequestOptions;
import java.util.List;

import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.R;
import valmir.com.offermarket.rest.ApiClient;


/**
 * Created by Besplay on 3/13/2017.
 */

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyProductHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


    private List<Oferta> productList;
    private Context context;


    public static class MyProductHolder extends RecyclerView.ViewHolder {

        private ImageView offerImage;
        private TextView url;
        private TextView title;
        private TextView skadon;
        ImageButton favButton;



        public MyProductHolder(View itemView) {
            super(itemView);

            offerImage = (ImageView) itemView.findViewById(R.id.offerImage);
//            url = (TextView) itemView.findViewById(R.id.url);
            title = (TextView) itemView.findViewById(R.id.title);
           // favButton = (ImageButton)itemView.findViewById(R.id.favButton);
            skadon = (TextView)itemView.findViewById(R.id.skadon);

            boolean fav = false;

        }
    }


    public OffersAdapter(List<Oferta> productList, Context context) {
        this.productList = productList;
        this.context = context;

    }


    @Override
    public MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offers, parent, false);
        return new MyProductHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final MyProductHolder holder, int position) {
        final Oferta product = productList.get(position);

       Typeface latoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
       Typeface latoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
       Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");



        Glide
                .with(context)
                .load(product.getLogo())
                .apply(new RequestOptions().placeholder(R.drawable.img_placeholder).fitCenter().dontTransform())
                .into(holder.offerImage);


            holder.skadon.setText("Skadon "+product.getSkadon());



       // holder.title.setTypeface(latoRegular);
        holder.title.setText(product.getTitle());




    }



    @Override
    public int getItemCount() {
        return productList.size();
    }


}
