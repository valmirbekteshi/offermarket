package valmir.com.offermarket.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import org.w3c.dom.Text;

import java.util.List;
import valmir.com.offermarket.R;
import valmir.com.offermarket.model.Header;
import valmir.com.offermarket.model.Oferta;

/**
 * Created by Administrator on 30/06/2015.
 */
public class OfferHeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int TYPE_HEADER = 0;
  private static final int TYPE_ITEM = 1;

  Header header;
  List<Oferta> listItems;
  Context context;

  public OfferHeaderAdapter(Context context,Header header, List<Oferta> listItems)
  {
    this.context  = context;
    this.header = header;
    this.listItems = listItems;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if(viewType == TYPE_HEADER)
    {
      View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_header, parent, false);
      return  new VHHeader(v);
    }
    else if(viewType == TYPE_ITEM)
    {
      View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offers, parent, false);
      return new VHItem(v);
    }
    return null;
  }

  private Oferta getItem(int position)
  {
    return listItems.get(position);
  }


  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");

    if(holder instanceof VHHeader)
    {
      VHHeader VHheader = (VHHeader)holder;
      VHheader.txtTitle.setText(header.getHeader());
    }
    else if(holder instanceof VHItem)
    {
      Oferta currentItem = getItem(position-1);
      VHItem VHitem = (VHItem)holder;
      VHitem.title.setText(currentItem.getTitle());

      Glide
          .with(context)
          .load(currentItem.getLogo())
          .apply(new RequestOptions().fitCenter().dontTransform())
          .into(VHitem.offerImage);

      VHitem.title.setTypeface(AEH);

      VHitem.skadon.setText("Skadon më \n"+currentItem.getSkadon());
    }
  }

  //    need to override this method
  @Override
  public int getItemViewType(int position) {
    if(isPositionHeader(position))
      return TYPE_HEADER;
    return TYPE_ITEM;
  }

  private boolean isPositionHeader(int position)
  {
    return position == 0;
  }

  @Override
  public int getItemCount() {
    return listItems.size()+1;
  }

  class VHHeader extends RecyclerView.ViewHolder{
    TextView txtTitle;
    public VHHeader(View itemView) {
      super(itemView);
      this.txtTitle = (TextView)itemView.findViewById(R.id.txtHeader);
    }
  }

  class VHItem extends RecyclerView.ViewHolder{
    private ImageView offerImage;
    private TextView url;
    private TextView title;
    private TextView skadon;
    public VHItem(View itemView) {
      super(itemView);
      offerImage = (ImageView) itemView.findViewById(R.id.offerImage);
      //            url = (TextView) itemView.findViewById(R.id.url);
      title = (TextView) itemView.findViewById(R.id.title);
      // favButton = (ImageButton)itemView.findViewById(R.id.favButton);
      skadon = (TextView)itemView.findViewById(R.id.skadon);
    }
  }
}