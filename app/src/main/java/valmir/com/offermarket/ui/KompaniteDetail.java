package valmir.com.offermarket.ui;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.fragments.KompaniDetail.InfoFragment;
import valmir.com.offermarket.fragments.KompaniDetail.KompaniteDetailAdapter;
import valmir.com.offermarket.fragments.KompaniDetail.OfertatFragment;
import valmir.com.offermarket.helpers.Var;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.rest.ApiClient;

public class KompaniteDetail extends AppCompatActivity {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bizneset_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);


        ViewPager vp_pages = (ViewPager) findViewById(R.id.vp_pages);
        PagerAdapter pagerAdapter = new KompaniteDetailAdapter(getSupportFragmentManager());
        vp_pages.setAdapter(pagerAdapter);

        toolbar.setTitle("Kompania");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ;// TODO: 17-May-18
                finish();
            }
        });



        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(vp_pages);


//
//        Bundle bundle = new Bundle();
//        bundle.putString("business_id","test");
//
//        OfertatFragment ofertatFragment = new OfertatFragment();
//        ofertatFragment.setArguments(bundle);


    }

    private void fetchOfferById(String offer_id) {
        Call<Oferta> call = apiService.getOfferById(offer_id);
        call.enqueue(new Callback<Oferta>() {
            @Override
            public void onResponse(Call<Oferta> call, Response<Oferta> response) {
                if (response.isSuccessful()) {

                    Oferta oferta = response.body();

                    if (oferta != null) {
                        // toolbar.setTitle("Ofertat nga "+productList.get(0).getOfertuesi());

                    }
                }
            }

            @Override
            public void onFailure(Call<Oferta> call, Throwable t) {

            }
        });
    }
}
