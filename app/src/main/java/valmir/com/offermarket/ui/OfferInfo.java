package valmir.com.offermarket.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.security.MessageDigest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.R;
import valmir.com.offermarket.helpers.Var;
import valmir.com.offermarket.rest.ApiClient;

public class OfferInfo extends AppCompatActivity implements OnMapReadyCallback {

  private GoogleMap mMap;
  ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
  String offer_id;

  TextView infoOrari;
  TextView infoWebsite;
  TextView infoTel;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_offer_info);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);


    SupportMapFragment mapFragment =
            (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    infoOrari = (TextView) findViewById(R.id.infoOrari);
    infoWebsite = (TextView) findViewById(R.id.infoWebsite);
    infoTel = (TextView) findViewById(R.id.infoTel);


    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    Intent intent = getIntent();
    Bundle extras = intent.getExtras();
    offer_id = extras.getString("offer_id");
    fetchOfferById(offer_id);
  }

  public void fetchOfferById(String id) {
    Call<Oferta> call = apiService.getOfferById(id);
    call.enqueue(new Callback<Oferta>() {
      @Override
      public void onResponse(Call<Oferta> call, Response<Oferta> response) {
        if (response.isSuccessful()) {

          Oferta oferta = response.body();

          if (oferta != null) {

            infoOrari.setText(oferta.getOrariPunes());
            infoWebsite.setText(oferta.getWebsite());
            infoTel.setText(oferta.getNrTel());


            getSupportActionBar().setTitle(oferta.getTitle());

            if (Var.notEmpty(oferta.getLatitude())
                    && Var.notEmpty(oferta.getLontitude())
                    && Var.notEmpty(oferta.getOfertuesi())
                    &&
                    Var.notEmpty(oferta.getLogo())) {

              createMarker(Var.toDouble(oferta.getLatitude()), Var.toDouble(oferta.getLontitude()),
                      oferta.getOfertuesi(), oferta.getLogo());
            }
          }
        }
      }

      @Override
      public void onFailure(Call<Oferta> call, Throwable t) {

      }
    });
  }


  protected void createMarker(double latitude, double longitude, String companieName, String logo) {

    MarkerOptions markerOptions =
            new MarkerOptions().position(new LatLng(latitude, longitude)).title(companieName);

    Marker marker = mMap.addMarker(markerOptions);
    marker.setTag(companieName);
    loadMarkerIcon(marker, logo);


    LatLng location = new LatLng(latitude, longitude);
    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 17.5f));
  }


  private void loadMarkerIcon(final Marker marker, String url) {
    Glide.with(this).asBitmap().load(url).apply(new RequestOptions().circleCropTransform().override(250, 130))
            .into(new SimpleTarget<Bitmap>() {
              @Override
              public void onResourceReady(@NonNull Bitmap bitmap,
                                          @Nullable Transition<? super Bitmap> transition) {
                BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);
                marker.setIcon(icon);
              }


            });

  }




}
