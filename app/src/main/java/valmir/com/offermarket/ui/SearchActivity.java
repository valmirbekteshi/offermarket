package valmir.com.offermarket.ui;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.adapter.OffersAdapter;
import valmir.com.offermarket.fragments.OffersFragment;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.rest.ApiClient;
import valmir.com.offermarket.ui.OfferDetailActivity;

public class SearchActivity extends AppCompatActivity {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    OffersAdapter offersAdapter;
    List<Oferta> productList;
    private SwipeRefreshLayout swipeRefreshLayout;
   Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);

        toolbar.setTitle("Search");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




        Intent intent = getIntent();

        String query = intent.getStringExtra("query");
        Toast.makeText(getApplicationContext(), query, Toast.LENGTH_SHORT).show();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                final Oferta product = productList.get(position);

                                Intent i = new Intent(getApplicationContext(), OfferDetailActivity.class);
                                i.putExtra("offer_id", product.getId());
                                i.putExtra("status", "offer");
                                startActivity(i);


                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));

        fetchOffersFromSearch(query);


    }

    public void fetchOffersFromSearch(String query) {
        Call<List<Oferta>> call = apiService.fetchOffers(query);
        call.enqueue(new Callback<List<Oferta>>() {
            @Override
            public void onResponse(Call<List<Oferta>> call, Response<List<Oferta>> response) {
                if (response.isSuccessful()) {
                    swipeRefreshLayout.setRefreshing(false);
                    productList = response.body();

                    offersAdapter = new OffersAdapter(productList, getApplicationContext());
                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);

                    recyclerView.setLayoutManager(staggeredGridLayoutManager);

                    recyclerView.setAdapter(offersAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Oferta>> call, Throwable t) {

            }
        });
    }


}
