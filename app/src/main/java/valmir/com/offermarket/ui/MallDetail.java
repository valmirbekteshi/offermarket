package valmir.com.offermarket.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.fragments.OffersFragment;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.model.Company;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.rest.ApiClient;

public class MallDetail extends AppCompatActivity {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private static final String TAG = OffersFragment.class.getSimpleName();
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    MallDetailAdater offersAdapter;
    List<Oferta> productList;
    private SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;
    private int category_id;
    String offer_cat_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mall_detail);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        category_id = intent.getIntExtra("mall_id", 1);
        offer_cat_name = intent.getStringExtra("offer_cat_name");
        toolbar.setTitle("Ofertat nga Qendra Tregtare " + offer_cat_name+"");
        fetchMallById(category_id);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                final Oferta product = productList.get(position);

                                Intent i = new Intent(getApplicationContext(), OfferDetailActivity.class);
                                i.putExtra("offer_id", product.getId());
                                i.putExtra("status","offer");
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchMallById(category_id);
            }
        });

    }

    void fetchMallById(int id ){

        Call<List<Oferta>> call = apiService.getOfferByMall(category_id);
        call.enqueue(new Callback<List<Oferta>>() {
            @Override
            public void onResponse(Call<List<Oferta>> call, Response<List<Oferta>> response) {
                if (response.isSuccessful()) {

                    if (!response.body().isEmpty()) {
                        swipeRefreshLayout.setRefreshing(false);
                        productList = response.body();

                        offersAdapter =
                                new MallDetailAdater(productList, getApplicationContext().getApplicationContext());
                        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
                        recyclerView.setLayoutManager(staggeredGridLayoutManager);
                        recyclerView.setAdapter(offersAdapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Oferta>> call, Throwable t) {
                Log.v(TAG, "FAIL fetching API");
                Toast.makeText(getApplicationContext().getApplicationContext(), t.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }


    class MallDetailAdater extends RecyclerView.Adapter<MallDetailAdater.MyProductHolder> {


        private List<Oferta> productList;
        private Context context;


        public class MyProductHolder extends RecyclerView.ViewHolder {

            private ImageView biznesiLogo;
            private TextView url;
            private TextView title;

            ImageButton favButton;


            public MyProductHolder(View itemView) {
                super(itemView);

                biznesiLogo = (ImageView) itemView.findViewById(R.id.offerImage);
                //            url = (TextView) itemView.findViewById(R.id.url);
                title = (TextView) itemView.findViewById(R.id.title);
                //favButton = (ImageButton)itemView.findViewById(R.id.favButton);


                boolean fav = false;

            }
        }


        public MallDetailAdater(List<Oferta> productList, Context context) {
            this.productList = productList;
            this.context = context;

        }


        @Override
        public MallDetailAdater.MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_malls, parent, false);
            return new MallDetailAdater.MyProductHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final MallDetailAdater.MyProductHolder holder, int position) {
            final Oferta product = productList.get(position);

            Typeface latoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            Typeface latoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
            Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");


            Glide
                    .with(context)
                    .load(product.getLogo())
                    .apply(new RequestOptions().fitCenter().dontTransform())
                    .into(holder.biznesiLogo);

            holder.title.setText(product.getTitle());


        }


        @Override
        public int getItemCount() {
            return productList.size();
        }


    }
}
