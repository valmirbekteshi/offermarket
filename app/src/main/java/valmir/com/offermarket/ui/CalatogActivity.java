package valmir.com.offermarket.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.fragments.OffersFragment;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.model.Catalog;
import valmir.com.offermarket.rest.ApiClient;

public class CalatogActivity extends AppCompatActivity {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private static final String TAG = OffersFragment.class.getSimpleName();
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    CatalogAdapter catalogAdapter;
    List<Catalog> productList;
    private SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calatog);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        toolbar.setTitle("Katalogu");

        fetchCatalogs();


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                final Catalog product = productList.get(position);

                                Intent i = new Intent(getApplicationContext(), OfferDetailActivity.class);
                                i.putExtra("catalog_id", product.getId());
                                i.putExtra("status", "catalog");
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchCatalogs();
            }
        });



        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void fetchCatalogs() {
        Call<List<Catalog>> call  = apiService.getKatalogat();
        call.enqueue(new Callback<List<Catalog>>() {
            @Override
            public void onResponse(Call<List<Catalog>> call, Response<List<Catalog>> response) {
                if(response.body().size()>0){

                    productList = response.body();
                    swipeRefreshLayout.setRefreshing(false);

                    catalogAdapter =
                            new CatalogAdapter(productList, getApplicationContext());
                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);
                    recyclerView.setAdapter(catalogAdapter);


                }
            }

            @Override
            public void onFailure(Call<List<Catalog>> call, Throwable t) {

            }
        });
    }


    class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.MyProductHolder> {




        private List<Catalog> productList;
        private Context context;


        public  class MyProductHolder extends RecyclerView.ViewHolder {

            private ImageView biznesiLogo;
            private TextView url;
            private TextView title;

            ImageButton favButton;



            public MyProductHolder(View itemView) {
                super(itemView);

                biznesiLogo = (ImageView) itemView.findViewById(R.id.offerImage);
                //            url = (TextView) itemView.findViewById(R.id.url);
                title = (TextView) itemView.findViewById(R.id.title);
                //favButton = (ImageButton)itemView.findViewById(R.id.favButton);


                boolean fav = false;

            }
        }


        public CatalogAdapter(List<Catalog> productList, Context context) {
            this.productList = productList;
            this.context = context;

        }


        @Override
        public CatalogAdapter.MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offers, parent, false);
            return new CatalogAdapter.MyProductHolder(itemView);
        }



        @Override
        public void onBindViewHolder(final CatalogAdapter.MyProductHolder holder, int position) {
            final Catalog product = productList.get(position);

            Typeface latoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            Typeface latoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
            Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");



            Glide
                    .with(context)
                    .load(product.getLogo())
                    .apply(new RequestOptions().placeholder(R.drawable.img_placeholder).fitCenter().dontTransform())
                    .into(holder.biznesiLogo);

            holder.title.setText(product.getTitle());


        }



        @Override
        public int getItemCount() {
            return productList.size();
        }


    }
}
