package valmir.com.offermarket.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.fragments.KompaniDetail.OfertatFragment;
import valmir.com.offermarket.fragments.OffersFragment;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.model.Company;
import valmir.com.offermarket.rest.ApiClient;

public class KompaniteActivity extends AppCompatActivity {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private static final String TAG = OffersFragment.class.getSimpleName();
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    BiznesetAdapter biznesetAdapter;
    List<Company> productList;
    private SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kompanite);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ;// TODO: 17-May-18
                finish();
            }
        });

        toolbar.setTitle("Kompanitë");


        fetchKompanite();


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                final Company product = productList.get(position);

                                Intent i = new Intent(getApplicationContext(), BrandOffers.class);
                                i.putExtra("brand_name", product.getName());
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchKompanite();
            }
        });


        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


    }

    private void fetchKompanite() {
        Call<List<Company>> call = apiService.getBizneset();
        call.enqueue(new Callback<List<Company>>() {
            @Override
            public void onResponse(Call<List<Company>> call, Response<List<Company>> response) {
                if (response.body().size() > 0) {

                    productList = response.body();
                    swipeRefreshLayout.setRefreshing(false);

                    biznesetAdapter =
                            new BiznesetAdapter(productList, getApplicationContext());
                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);
                    recyclerView.setAdapter(biznesetAdapter);


                }
            }

            @Override
            public void onFailure(Call<List<Company>> call, Throwable t) {

            }
        });
    }

    class BiznesetAdapter extends RecyclerView.Adapter<BiznesetAdapter.MyProductHolder> {


        private List<Company> productList;
        private Context context;


        public class MyProductHolder extends RecyclerView.ViewHolder {

            private ImageView biznesiLogo;
            private TextView url;
            private TextView title;

            ImageButton favButton;


            public MyProductHolder(View itemView) {
                super(itemView);

                biznesiLogo = (ImageView) itemView.findViewById(R.id.offerImage);
                //            url = (TextView) itemView.findViewById(R.id.url);
                title = (TextView) itemView.findViewById(R.id.title);
                //favButton = (ImageButton)itemView.findViewById(R.id.favButton);


                boolean fav = false;

            }
        }


        public BiznesetAdapter(List<Company> productList, Context context) {
            this.productList = productList;
            this.context = context;

        }


        @Override
        public BiznesetAdapter.MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offers, parent, false);
            return new BiznesetAdapter.MyProductHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final BiznesetAdapter.MyProductHolder holder, int position) {
            final Company product = productList.get(position);

            Typeface latoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            Typeface latoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
            Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");


            Glide
                    .with(context)
                    .load(product.getLogo())
                    .apply(new RequestOptions().fitCenter().dontTransform())
                    .into(holder.biznesiLogo);

            holder.title.setText(product.getName());


        }


        @Override
        public int getItemCount() {
            return productList.size();
        }


    }

}
