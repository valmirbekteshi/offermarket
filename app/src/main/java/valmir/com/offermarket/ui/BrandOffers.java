package valmir.com.offermarket.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.R;
import valmir.com.offermarket.fragments.OffersFragment;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.rest.ApiClient;

public class BrandOffers extends AppCompatActivity {

  ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
  private static final String TAG = OffersFragment.class.getSimpleName();
  RecyclerView recyclerView;
  StaggeredGridLayoutManager staggeredGridLayoutManager;
  BrandOffersAdapter offersAdapter;
  List<Oferta> productList;
  private SwipeRefreshLayout swipeRefreshLayout;
  Toolbar toolbar;
  private String brand_offer = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_brand_offers);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
    recyclerView = (RecyclerView) findViewById(R.id.recycleView);

    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       finish();
      }
    });
    Intent intent = getIntent();
    brand_offer = intent.getStringExtra("brand_name");



    fetchBrandOffer(brand_offer);

    recyclerView.addOnItemTouchListener(
        new RecyclerItemClickListener(getApplicationContext(), recyclerView,
            new RecyclerItemClickListener.OnItemClickListener() {
              @Override
              public void onItemClick(View view, int position) {
                final Oferta product = productList.get(position);

                Intent i = new Intent(getApplicationContext(), OfferDetailActivity.class);
                i.putExtra("offer_id", product.getId());
                i.putExtra("status", "offer");
                startActivity(i);
              }

              @Override
              public void onLongItemClick(View view, int position) {

              }
            }));


    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        fetchBrandOffer(brand_offer);
      }
    });



    swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
        android.R.color.holo_green_light,
        android.R.color.holo_orange_light,
        android.R.color.holo_red_light);





  }

  public void fetchBrandOffer(  String brand) {

    Call<List<Oferta>> call = apiService.getOfferByBrand(brand);
    call.enqueue(new Callback<List<Oferta>>() {
      @Override
      public void onResponse(Call<List<Oferta>> call, Response<List<Oferta>> response) {
        if (response.isSuccessful()) {


          swipeRefreshLayout.setRefreshing(false);
          productList = response.body();
          toolbar.setTitle("Ofertat nga "+productList.get(0).getOfertuesi());
          offersAdapter =
              new BrandOffersAdapter(productList, getApplicationContext().getApplicationContext());
          staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
          recyclerView.setLayoutManager(staggeredGridLayoutManager);
          recyclerView.setAdapter(offersAdapter);
        }
      }

      @Override
      public void onFailure(Call<List<Oferta>> call, Throwable t) {
        Log.v(TAG, "FAIL fetching API");
        Toast.makeText(getApplicationContext().getApplicationContext(), "FAIL fetching API",
            Toast.LENGTH_LONG).show();
      }
    });
  }

  class BrandOffersAdapter extends RecyclerView.Adapter<BrandOffersAdapter.MyProductHolder> {




    private List<Oferta> productList;
    private Context context;


    public  class MyProductHolder extends RecyclerView.ViewHolder {

      private ImageView offerImage;
      private TextView url;
      private TextView title;
      private TextView skadon;
      ImageButton favButton;



      public MyProductHolder(View itemView) {
        super(itemView);

        offerImage = (ImageView) itemView.findViewById(R.id.offerImage);
        //            url = (TextView) itemView.findViewById(R.id.url);
        title = (TextView) itemView.findViewById(R.id.title);
        //favButton = (ImageButton)itemView.findViewById(R.id.favButton);
        skadon = (TextView)itemView.findViewById(R.id.skadon);

        boolean fav = false;

      }
    }


    public BrandOffersAdapter(List<Oferta> productList, Context context) {
      this.productList = productList;
      this.context = context;

    }


    @Override
    public MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offers, parent, false);
      return new BrandOffersAdapter.MyProductHolder(itemView);
    }



    @Override
    public void onBindViewHolder(final BrandOffersAdapter.MyProductHolder holder, int position) {
      final Oferta product = productList.get(position);

      Typeface latoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
      Typeface latoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
      Typeface AEH = Typeface.createFromAsset(context.getAssets(), "fonts/AEH.ttf");



      Glide
          .with(context)
          .load(product.getLogo())
          .apply(new RequestOptions().fitCenter().dontTransform())
          .into(holder.offerImage);

      holder.skadon.setText(product.getSkadon());
holder.title.setText(product.getTitle());

    }



    @Override
    public int getItemCount() {
      return productList.size();
    }


  }

}
