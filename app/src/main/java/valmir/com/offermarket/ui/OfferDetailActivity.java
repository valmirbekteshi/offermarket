package valmir.com.offermarket.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.loader.MediaLoader;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.model.Catalog;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.R;

import valmir.com.offermarket.helpers.Var;
import valmir.com.offermarket.rest.ApiClient;

public class OfferDetailActivity extends AppCompatActivity {


  String imgUrl;

  MenuItem prevMenuItem;
  private String offerId;
  ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
  private ScrollGalleryView scrollGalleryView;
  private String web_url = "";
  private String status;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_offer_detail);

    //imageSlider = (SliderLayout) findViewById(R.id.slider);


    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    toolbar.setTitle("");


    scrollGalleryView = (ScrollGalleryView) findViewById(R.id.scroll_gallery_view);


    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });


    LocalBroadcastManager.getInstance(this)
            .registerReceiver(mHandler, new IntentFilter("offer_detail"));



    Intent intent = getIntent();

    status = intent.getStringExtra("status");
   // offerId = intent.getStringExtra("offer_id");
    switch (status) {

      case "offer": {
        offerId = intent.getStringExtra("offer_id");
        fetchOfferById(offerId);
        break;
      }

      case "coupon": {
        offerId = intent.getStringExtra("offer_id");
        fetchOfferById(offerId);
        break;
      }


      case "catalog": {
        offerId = intent.getStringExtra("catalog_id");
        fetchCatalogById(offerId);
        break;
      }

    }

//    fetchOfferById(offerId);

  }



  public void fetchOfferById(String id) {
    Call<Oferta> call = apiService.getOfferById(id);
    call.enqueue(new Callback<Oferta>() {
      @Override
      public void onResponse(Call<Oferta> call, Response<Oferta> response) {
        if (response.isSuccessful()) {

          Oferta oferta = response.body();
          getSupportActionBar().setTitle(oferta.getTitle());
          web_url = oferta.getWebsite();


          if (Var.notEmpty(oferta.getImages())) {

            imgUrl = oferta.getImages();
            String[] words = imgUrl.split(";");//splits the string based on whitespace

            final ArrayList<String> images = new ArrayList<>();
            List<MediaInfo> infos = new ArrayList<>(words.length);
            for (final String url : words) infos.add(MediaInfo.mediaLoader(new MediaLoader() {
              @Override public boolean isImage() {
                return false;
              }

              @Override public void loadMedia(Context context, final ImageView imageView,
                                              final SuccessCallback callback) {

                Glide.with(getApplicationContext()).asBitmap().load(url).apply(new RequestOptions().centerCrop().placeholder(R.drawable.img_placeholder))
                        .into(new SimpleTarget<Bitmap>() {
                          @Override public void onResourceReady(@NonNull Bitmap bitmap,
                                                                @Nullable Transition<? super Bitmap> transition) {
                            //                BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);

                            imageView.setImageBitmap(bitmap);
                            callback.onSuccess();

                          }
                        });
              }

              @Override public void loadThumbnail(Context context, final ImageView thumbnailView,
                                                  final SuccessCallback callback) {


                Glide.with(getApplicationContext()).asBitmap().load(url)
                        .apply(new RequestOptions().fitCenter().placeholder(R.drawable.offermarket))
                        .into(new SimpleTarget<Bitmap>() {
                          @Override public void onResourceReady(@NonNull Bitmap bitmap,
                                                                @Nullable Transition<? super Bitmap> transition) {
                            //                BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);

                            thumbnailView.setImageBitmap(bitmap);
                            callback.onSuccess();

                          }
                        });
              }
            }));

            scrollGalleryView
                    .setThumbnailSize(150)
                    .setZoom(true)
                    .setFragmentManager(getSupportFragmentManager())
                    .addMedia(infos);



          }
        }
      }

      @Override
      public void onFailure(Call<Oferta> call, Throwable t) {

      }
    });
  }


  private void fetchCatalogById(String offerId) {

    Call<Catalog> call = apiService.getCatalogById(offerId);
    call.enqueue(new Callback<Catalog>() {
      @Override
      public void onResponse(Call<Catalog> call, Response<Catalog> response) {
        if (response.isSuccessful()) {

          Catalog oferta = response.body();
          getSupportActionBar().setTitle(oferta.getTitle());
          web_url = oferta.getWebsite();


          if (Var.notEmpty(oferta.getImages())) {

            imgUrl = oferta.getImages();
            String[] words = imgUrl.split(";");//splits the string based on whitespace

            final ArrayList<String> images = new ArrayList<>();
            List<MediaInfo> infos = new ArrayList<>(words.length);
            for (final String url : words) infos.add(MediaInfo.mediaLoader(new MediaLoader() {
              @Override public boolean isImage() {
                return false;
              }

              @Override public void loadMedia(Context context, final ImageView imageView,
                                              final SuccessCallback callback) {

                Glide.with(getApplicationContext()).asBitmap().load(url).apply(new RequestOptions().centerCrop().placeholder(R.drawable.offermarket))
                        .into(new SimpleTarget<Bitmap>() {
                          @Override public void onResourceReady(@NonNull Bitmap bitmap,
                                                                @Nullable Transition<? super Bitmap> transition) {
                            //                BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);

                            imageView.setImageBitmap(bitmap);
                            callback.onSuccess();

                          }
                        });
              }

              @Override public void loadThumbnail(Context context, final ImageView thumbnailView,
                                                  final SuccessCallback callback) {


                Glide.with(getApplicationContext()).asBitmap().load(url)
                        .apply(new RequestOptions().fitCenter().placeholder(R.drawable.offermarket))
                        .into(new SimpleTarget<Bitmap>() {
                          @Override public void onResourceReady(@NonNull Bitmap bitmap,
                                                                @Nullable Transition<? super Bitmap> transition) {
                            //                BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);

                            thumbnailView.setImageBitmap(bitmap);
                            callback.onSuccess();

                          }
                        });
              }
            }));

            scrollGalleryView
                    .setThumbnailSize(150)
                    .setZoom(true)
                    .setFragmentManager(getSupportFragmentManager())
                    .addMedia(infos);



          }
        }
      }

      @Override
      public void onFailure(Call<Catalog> call, Throwable t) {

      }
    });
  }





  private Bitmap toBitmap(int image) {
    return ((BitmapDrawable) getResources().getDrawable(image)).getBitmap();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_info) {

      Intent intent = new Intent(OfferDetailActivity.this,OfferInfo.class);
      intent.putExtra("offer_id", offerId);
      startActivity(intent);

      return true;
    }

    return super.onOptionsItemSelected(item);
  }



  private BroadcastReceiver mHandler = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {

      offerId = intent.getStringExtra("offer_id");
      status = intent.getStringExtra("status");
    }
  };

  @Override
  protected void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
  }
}
