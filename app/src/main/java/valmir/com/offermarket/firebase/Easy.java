package valmir.com.offermarket.firebase;

/**
 * Created by Valmir on 16-Mar-18.
 */

import java.io.IOException;
//import needle.Needle;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

public class Easy {


  private String ntitle;
  private String nbody;
  private String nclick_action;
  private String ntopic;
  private String nPrice;
  private String nToken;
  private String nsound;
  private String API_KEY;
  private Easy.EasyNotifyListener listener = null;
  private Response response;

  public String getnPrice() {
    return nPrice;
  }

  public void setnPrice(String nPrice) {
    this.nPrice = nPrice;
  }

  public String getnToken() {
    return nToken;
  }

  public void setnToken(String nToken) {
    this.nToken = nToken;
  }

  public void setNtitle(String ntitle) {
    this.ntitle = ntitle;
  }

  public void setNbody(String nbody) {
    this.nbody = nbody;
  }

  public void setNclick_action(String nclick_action) {
    this.nclick_action = nclick_action;
  }

  public void setNtopic(String ntopic) {
    this.ntopic = ntopic;
  }

  public void setAPI_KEY(String API_KEY) {
    this.API_KEY = API_KEY;
  }

  public void setNsound(String nsound) {
    this.nsound = nsound;
  }

  private void sendNotification() throws IOException, JSONException {
    OkHttpClient client = new OkHttpClient();
    MediaType mediaType = MediaType.parse("application/json");
    JSONObject notificationObject = new JSONObject();
    JSONObject dataObject = new JSONObject();
    JSONObject bodyObject = new JSONObject();

    //        notificationObject.put("title", this.ntitle).put("body", this.nbody).put("sound", this.nsound);
    dataObject.put("message", this.nbody);
    dataObject.put("title", this.ntitle);
    dataObject.put("click_action", this.nclick_action);
    dataObject.put("price", this.nPrice);
    dataObject.put("token", this.nToken);

    bodyObject.put("to", this.ntopic)
        .put("data", dataObject);
    //        .put("notification", notificationObject)

    RequestBody body = RequestBody.create(mediaType, bodyObject.toString());
    Request request = (new Builder()).url("https://fcm.googleapis.com/fcm/send")
        .post(body)
        .addHeader("content-type", "application/json")
        .addHeader("authorization", "key=" + this.API_KEY)
        .build();
    this.response = client.newCall(request).execute();
  }

  //public  void nPush() {
  //  new Thread(new Runnable() {
  //    @Override public void run() {
  //
  //      new AsyncTask<Void, Void, Void>() {
  //        protected void onPreExecute() {
  //          super.onPreExecute();
  //        }
  //
  //        protected Void doInBackground(Void... params) {
  //          try {
  //            Easy.this.sendNotification();
  //          } catch (IOException var3) {
  //            var3.printStackTrace();
  //          } catch (JSONException var4) {
  //            var4.printStackTrace();
  //          }
  //
  //          return null;
  //        }
  //
  //        protected void onPostExecute(Void aVoid) {
  //          String rc = null;
  //
  //          try {
  //            rc = Easy.this.response.body().string();
  //          } catch (IOException var4) {
  //            var4.printStackTrace();
  //          }
  //
  //          if (Easy.this.response.code() == 200) {
  //            Easy.this.listener.onNotifySuccess(rc);
  //          } else {
  //            Easy.this.listener.onNotifyError(rc);
  //          }
  //
  //          super.onPostExecute(aVoid);
  //        }
  //      }.execute(new Void[0]);
  //
  //    }
  //  });
  //}




      public void push()
      {
        //
        //Needle.onBackgroundThread().execute(new Runnable() {
        //  @Override
        //  public void run() {
        //    try {
        //      Easy.this.sendNotification();
        //    } catch (IOException var3) {
        //      var3.printStackTrace();
        //    } catch (JSONException var4) {
        //      var4.printStackTrace();
        //    }
        //  }
        //});



      }


  public Easy() {
  }

  public void setEasyNotifyListener(Easy.EasyNotifyListener listener) {
    this.listener = listener;
  }

  public interface EasyNotifyListener {
    void onNotifySuccess(String var1);

    void onNotifyError(String var1);
  }
}
