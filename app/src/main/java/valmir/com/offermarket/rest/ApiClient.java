package valmir.com.offermarket.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Valmir on 3/27/2017.
 */

public class ApiClient {


    //Emulator
  //  public static  final String BASE_URL = "http://10.0.2.2:3000/api/";


    //Phone
     public static  final String BASE_URL = "http://offermarket.us.openode.io/api/";

    private  static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}