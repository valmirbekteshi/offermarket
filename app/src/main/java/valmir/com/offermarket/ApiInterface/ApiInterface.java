package valmir.com.offermarket.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Path;
import valmir.com.offermarket.model.Company;
import valmir.com.offermarket.model.Catalog;
import valmir.com.offermarket.model.Categories;
import valmir.com.offermarket.model.Mall;
import valmir.com.offermarket.model.Oferta;

public interface ApiInterface {

  @GET("offers")
  Call<List<Oferta>> getOffers();

  @GET("companies")
  Call<List<Company>> getBizneset();

  @GET("mall")
  Call<List<Mall>> getMalls();

  @GET("catalogs")
  Call<List<Catalog>> getKatalogat();

  @GET("search/{query}")
  Call<List<Oferta>> fetchOffers(@Path("query") String query);


  /// GET BY ID
  @GET("offers/{id}")
  Call<Oferta> getOfferById(@Path("id") String id);

  @GET("mall/{id}")
  Call<Mall> getMallById(@Path("id") String id);

  @GET("companies/{id}")
  Call<Company> getCompanyById(@Path("id") String id);

  @GET("catalogs/{id}")
  Call<Catalog> getCatalogById(@Path("id") String id);


  @GET("getOfferByBrand/{brand}")
  Call<List<Oferta>> getOfferByBrand(@Path("brand") String brand);

  @GET("getOfferByMall/{mallId}")
  Call<List<Oferta>> getOfferByMall(@Path("mallId") int mallId);


  @GET("getOfferByCategory/{id}")
  Call<List<Oferta>> getOfferByCategoryId(@Path("id") int id);

  @GET("categories")
  Call<List<Categories>> getCategories();

  //    @PUT("offers/{id}")
  //    Call<ResponseBody> updateOffer(@Path("id") String id, @Body Oferta oferta);


}
