package valmir.com.offermarket.fragments.KompaniDetail;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import valmir.com.offermarket.fragments.CategoriesFragment;
import valmir.com.offermarket.fragments.HartaFragment;
import valmir.com.offermarket.fragments.OffersFragment;

public class KompaniteDetailAdapter extends FragmentPagerAdapter {

    public KompaniteDetailAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new OfertatFragment();
            case 1:
                return new InfoFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            //
            //Your tab titles
            //
            case 0:
                return "Ofertat";
            case 1:
                return "Info";
            default:
                return null;
        }
    }

}