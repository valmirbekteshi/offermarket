package valmir.com.offermarket.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.model.Company;
import valmir.com.offermarket.rest.ApiClient;
import valmir.com.offermarket.ui.BrandOffers;

/**
 * Created by Valmir on 12-Nov-17.
 */

public class HartaFragment extends Fragment
    implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

  private GoogleMap mMap;
  ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

  List<Company> productList;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);


  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    productList = new ArrayList<>();
    View rootView = inflater.inflate(R.layout.fragment_harta, container, false);
    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
        .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    return rootView;
  }

  public void getAllBizneset() {

    Call<List<Company>> call = apiService.getBizneset();
    call.enqueue(new Callback<List<Company>>() {
      @Override
      public void onResponse(Call<List<Company>> call, Response<List<Company>> response) {
        if (response.isSuccessful()) {

          productList = response.body();

          for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getLatitude() != 0
                && productList.get(i).getLontitude() != 0) {
              createMarker(productList.get(i).getLatitude(),
                      productList.get(i).getLontitude(),
                      productList.get(i).getName(), productList.get(i).getId(),
                      productList.get(i).getLogo());


            }
          }

          LatLng location = new LatLng(productList.get(productList.size()-1).getLatitude(),
                  productList.get(productList.size()-1).getLontitude());
          mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 7.5f));




        }
      }

      @Override
      public void onFailure(Call<List<Company>> call, Throwable t) {

        Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override public void onMapReady(GoogleMap map) {
    mMap = map;

    getAllBizneset();
    //if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
    //    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
    //    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
    //
    //}

    mMap.getUiSettings().setZoomControlsEnabled(true);
    mMap.getUiSettings().setCompassEnabled(true);
    mMap.setOnMarkerClickListener(this);
  }

  private void loadMarkerIcon(final Marker marker, String url) {
    Glide.with(this).asBitmap().load(url).apply(new RequestOptions().fitCenter().transform(new CircleCrop())
            .placeholder(R.drawable.img_placeholder).
            override(380,200))
        .into(new SimpleTarget<Bitmap>() {
          @Override public void onResourceReady(@NonNull Bitmap bitmap,
              @Nullable Transition<? super Bitmap> transition) {
            BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);
            marker.setIcon(icon);

          }
        });

  }


  protected void createMarker(double latitude, double longitude, String companieName, String tag, String logo) {

    MarkerOptions markerOptions =
        new MarkerOptions().position(new LatLng(latitude, longitude)).title(companieName);

    Marker marker = mMap.addMarker(markerOptions);
    marker.setTag(companieName);
    loadMarkerIcon(marker, logo);
  }

  @Override
  public boolean onMarkerClick(Marker marker) {

    Intent intent = new Intent(getContext(), BrandOffers.class);
    intent.putExtra("brand_name", marker.getTag().toString().trim());
    startActivity(intent);

    return false;
  }

  public HartaFragment() {
    // Required empty public constructor
  }
}
