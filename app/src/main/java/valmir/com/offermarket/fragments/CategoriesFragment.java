package valmir.com.offermarket.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.model.Categories;
import valmir.com.offermarket.adapter.CategoriesAdapter;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.rest.ApiClient;
import valmir.com.offermarket.ui.CategoryOffers;
import valmir.com.offermarket.ui.OfferDetailActivity;

/**
 * Created by Valmir on 12-Nov-17.
 */

public class CategoriesFragment extends Fragment {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private static final String TAG = CategoriesFragment.class.getSimpleName();
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    CategoriesAdapter categoriesAdapter;
    List<Categories> categoriesList;
    private SwipeRefreshLayout swipeRefreshLayout;


    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d("Token", token);
        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchCompanies();
            }
        });

        fetchCompanies();

        recyclerView.addOnItemTouchListener(
            new RecyclerItemClickListener(getContext().getApplicationContext(), recyclerView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final Categories product = categoriesList.get(position);

                        Intent i = new Intent(getContext().getApplicationContext(), CategoryOffers.class);
                        i.putExtra("category_id", product.getCategoryID());
                        i.putExtra("offer_cat_name",product.getName());
                        startActivity(i);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        return rootView;
    }

    public void fetchCompanies() {
        Call<List<Categories>> call = apiService.getCategories();
        call.enqueue(new Callback<List<Categories>>() {
            @Override
            public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response) {
                if (response.isSuccessful()) {
                    swipeRefreshLayout.setRefreshing(false);
                    categoriesList = response.body();
//                    productList = fetchOfertat();
                    Log.v(TAG, "Succes fetching API" + categoriesList);
                    categoriesAdapter = new CategoriesAdapter(categoriesList, getContext().getApplicationContext());
                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);
                    recyclerView.setAdapter(categoriesAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Categories>> call, Throwable t) {
                Log.v(TAG, "FAIL fetching API");
                Toast.makeText(getContext().getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });

    }

}