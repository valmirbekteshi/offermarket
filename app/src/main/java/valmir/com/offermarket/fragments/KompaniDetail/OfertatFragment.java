package valmir.com.offermarket.fragments.KompaniDetail;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.adapter.OffersAdapter;
import valmir.com.offermarket.fragments.OffersFragment;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.rest.ApiClient;
import valmir.com.offermarket.ui.BrandOffers;
import valmir.com.offermarket.ui.OfferDetailActivity;

public class OfertatFragment extends Fragment {

    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private static final String TAG = OffersFragment.class.getSimpleName();
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    OffersAdapter offersAdapter;
    List<Oferta> productList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String brand_offer;


    public OfertatFragment() {
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ofertat_fragment, container, false);


        Bundle bundle = getArguments();
        String id = bundle.getString("business_id");
        Toast.makeText(getContext(), id, Toast.LENGTH_SHORT).show();

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);


        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);

        Intent intent = getActivity().getIntent();


        brand_offer = intent.getStringExtra("brand_name");

        fetchBrandOffer(brand_offer);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchBrandOffer(brand_offer);
            }
        });


        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                final Oferta product = productList.get(position);

                                Intent i = new Intent(getContext(), OfferDetailActivity.class);
                                i.putExtra("offer_id", product.getId());
                                i.putExtra("status", "offer");
                                startActivity(i);


                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));


        return rootView;
    }

    public void fetchBrandOffer(String brand) {

        Call<List<Oferta>> call = apiService.getOfferByBrand(brand);
        call.enqueue(new Callback<List<Oferta>>() {
            @Override
            public void onResponse(Call<List<Oferta>> call, Response<List<Oferta>> response) {
                if (response.isSuccessful()) {


                    swipeRefreshLayout.setRefreshing(false);
                    productList = response.body();

                    offersAdapter = new OffersAdapter(productList, getContext());
                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
                    gridLayoutManager = new GridLayoutManager(getContext(), 2);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);


                    recyclerView.setAdapter(offersAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Oferta>> call, Throwable t) {
                Log.v(TAG, "FAIL fetching API");

            }
        });
    }
}
