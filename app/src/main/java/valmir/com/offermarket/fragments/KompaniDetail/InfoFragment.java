package valmir.com.offermarket.fragments.KompaniDetail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.R;
import valmir.com.offermarket.helpers.Var;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.rest.ApiClient;

public class InfoFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    String offer_id;
    TextView orari;


    public InfoFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)

    {
        View rootView = inflater.inflate(R.layout.info_fragment, container, false);


//        getActivity().setSupportActionBar(toolbar);


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        orari = (TextView) rootView.findViewById(R.id.infoOrari);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();
        offer_id = extras.getString("offer_id");
        fetchOfferById(offer_id);
    }

    public void fetchOfferById(String id) {
        Call<Oferta> call = apiService.getOfferById(id);
        call.enqueue(new Callback<Oferta>() {
            @Override
            public void onResponse(Call<Oferta> call, Response<Oferta> response) {
                if (response.isSuccessful()) {

                    Oferta oferta = response.body();

                    if (oferta != null) {

                        orari.setText(oferta.getOrariPunes());


                        if (Var.notEmpty(oferta.getLatitude())
                                && Var.notEmpty(oferta.getLontitude())
                                && Var.notEmpty(oferta.getOfertuesi())
                                &&
                                Var.notEmpty(oferta.getLogo())) {

                            createMarker(Var.toDouble(oferta.getLatitude()), Var.toDouble(oferta.getLontitude()),
                                    oferta.getOfertuesi(), oferta.getLogo());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Oferta> call, Throwable t) {

            }
        });
    }


    protected void createMarker(double latitude, double longitude, String companieName, String logo) {

        MarkerOptions markerOptions =
                new MarkerOptions().position(new LatLng(latitude, longitude)).title(companieName);

        Marker marker = mMap.addMarker(markerOptions);
        marker.setTag(companieName);
        loadMarkerIcon(marker, logo);


        LatLng location = new LatLng(latitude, longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 7.5f));
    }


    private void loadMarkerIcon(final Marker marker, String url) {
        Glide.with(this).asBitmap().load(url).apply(new RequestOptions().fitCenter().override(350, 200))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap bitmap,
                                                @Nullable Transition<? super Bitmap> transition) {
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);
                        marker.setIcon(icon);
                    }
                });

    }

}
