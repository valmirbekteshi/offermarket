package valmir.com.offermarket.fragments;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by Valmir on 12-Nov-17.
 */

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import valmir.com.offermarket.ApiInterface.ApiInterface;
import valmir.com.offermarket.adapter.OffersAdapter;
import valmir.com.offermarket.model.Header;
import valmir.com.offermarket.model.Oferta;
import valmir.com.offermarket.R;
import valmir.com.offermarket.adapter.OffersAdapter;
import valmir.com.offermarket.ui.MainActivity;
import valmir.com.offermarket.ui.OfferDetailActivity;
import valmir.com.offermarket.helpers.RecyclerItemClickListener;
import valmir.com.offermarket.rest.ApiClient;

import static android.content.Context.SEARCH_SERVICE;

public class OffersFragment extends Fragment {

  ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
  private static final String TAG = OffersFragment.class.getSimpleName();
  RecyclerView recyclerView;
  StaggeredGridLayoutManager staggeredGridLayoutManager;
  LinearLayoutManager linearLayoutManager;
  GridLayoutManager gridLayoutManager;
  OffersAdapter offersAdapter;
  List<Oferta> productList;
  private SwipeRefreshLayout swipeRefreshLayout;


  public OffersFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View rootView = inflater.inflate(R.layout.fragment_offers, container, false);
    swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);



    recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);



//
//    LocalBroadcastManager.getInstance(getContext())
//        .registerReceiver(mHandler, new IntentFilter("priceConfirm"));


    fetchOffers();

    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        fetchOffers();
      }
    });




    swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
        android.R.color.holo_green_light,
        android.R.color.holo_orange_light,
        android.R.color.holo_red_light);

    recyclerView.addOnItemTouchListener(
        new RecyclerItemClickListener(getContext(), recyclerView,
            new RecyclerItemClickListener.OnItemClickListener() {
              @Override
              public void onItemClick(View view, int position) {
                final Oferta product = productList.get(position);

                Intent i = new Intent(getContext(), OfferDetailActivity.class);
                i.putExtra("offer_id", product.getId());
                i.putExtra("status","offer");
                startActivity(i);


              }

              @Override
              public void onLongItemClick(View view, int position) {

              }
            }));

    return rootView;
  }




  public void fetchOffers() {
    Call<List<Oferta>> call = apiService.getOffers();
    call.enqueue(new Callback<List<Oferta>>() {
      @Override
      public void onResponse(Call<List<Oferta>> call, Response<List<Oferta>> response) {
        if (response.isSuccessful()) {
          swipeRefreshLayout.setRefreshing(false);
          productList = response.body();

          offersAdapter = new OffersAdapter( productList,getContext());
          staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
          gridLayoutManager = new GridLayoutManager(getContext(),2);
          recyclerView.setLayoutManager(staggeredGridLayoutManager);


          recyclerView.setAdapter(offersAdapter);
        }
      }

      @Override
      public void onFailure(Call<List<Oferta>> call, Throwable t) {
        Log.v(TAG, "FAIL fetching API");

      }
    });
  }

  private BroadcastReceiver mHandler = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {

    }
  };



}